<?php

/**
 * @file
 * uw_auth_cas_common.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_auth_cas_common_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_open-authentication:admin/config/system/uw_auth_cas_common_settings.
  $menu_links['menu-site-management_open-authentication:admin/config/system/uw_auth_cas_common_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/uw_auth_cas_common_settings',
    'router_path' => 'admin/config/system/uw_auth_cas_common_settings',
    'link_title' => 'Open authentication',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_open-authentication:admin/config/system/uw_auth_cas_common_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Open authentication');

  return $menu_links;
}
