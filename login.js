/**
 * @file
 */

(function ($) {
  Drupal.behaviors.uw_auth_cas_common = {
    attach: function (context) {
      if ($("#edit-cas-identifier").attr("checked")) {
        $('#edit-submit', context).val('Log in via CAS');
      }
      else {
        $('#edit-submit', context).val('Log in locally');
      }
      $('li.cas-link', context)
        .click(function () {
          $('#edit-submit', context).val('Log in via CAS');
          return false;
        });
      $('li.uncas-link', context)
        .click(function () {
          $('#edit-submit', context).val('Log in locally');
          return false;
        });
    }
  };
})(jQuery);
