<?php

/**
 * @file
 * uw_auth_cas_common.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_auth_cas_common_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'cas_link';
  $context->description = 'Adds a CAS login/logout link';
  $context->tag = 'Navigation';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_auth_cas_common-cas' => array(
          'module' => 'uw_auth_cas_common',
          'delta' => 'cas',
          'region' => 'login_link',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Adds a CAS login/logout link');
  t('Navigation');
  $export['cas_link'] = $context;

  return $export;
}
