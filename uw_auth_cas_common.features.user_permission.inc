<?php

/**
 * @file
 * uw_auth_cas_common.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_auth_cas_common_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer authentication configuration'.
  $permissions['administer authentication configuration'] = array(
    'name' => 'administer authentication configuration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'uw_auth_cas_common',
  );

  // Exported permission: 'administer realname'.
  $permissions['administer realname'] = array(
    'name' => 'administer realname',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'realname',
  );

  // Exported permission: 'create field_uw_user_guid'.
  $permissions['create field_uw_user_guid'] = array(
    'name' => 'create field_uw_user_guid',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_uw_user_guid'.
  $permissions['edit field_uw_user_guid'] = array(
    'name' => 'edit field_uw_user_guid',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_uw_user_guid'.
  $permissions['edit own field_uw_user_guid'] = array(
    'name' => 'edit own field_uw_user_guid',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view authentication configuration'.
  $permissions['view authentication configuration'] = array(
    'name' => 'view authentication configuration',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_auth_cas_common',
  );

  // Exported permission: 'view field_uw_user_guid'.
  $permissions['view field_uw_user_guid'] = array(
    'name' => 'view field_uw_user_guid',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_uw_user_guid'.
  $permissions['view own field_uw_user_guid'] = array(
    'name' => 'view own field_uw_user_guid',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  return $permissions;
}
